#!/usr/bin/env python
from multiprocessing import Process

import json
import os
import platform
import shutil
import sys
import tarfile
import zipfile

if sys.version_info > (3,0):
	import urllib.request as urlib
else:
	import urllib2 as urlib


def get_latest_electron_version():
	releases_uri = "https://api.github.com/repos/atom/electron/releases"

	response = urlib.urlopen(releases_uri)
	data = json.load(response)

	latest_release_tag = data[0]['tag_name']
	latest_version = latest_release_tag[1:] 		# Cut off the v from vx.y.z

	return latest_version

def make_tar(build, output):
	with tarfile.open(output, "w:gz") as tar:
		tar.add(build, arcname=os.path.basename(build))

def make_zip(build, output):
	zippy = zipfile.ZipFile(output, 'w')
	for root, dirs, files in os.walk(build):
		for file in files:
			zippy.write(os.path.join(root, file))
	zippy.close()
	return zippy

def compress_builds():
	# Linux
	linux32 = Process(target=make_tar, args=("Possel-linux-ia32", "possel-linux-x32.tar.gz"))
	linux64 = Process(target=make_tar, args=("Possel-linux-x64", "possel-linux-x64.tar.gz"))
	# OS X
	osx64 = Process(target=make_tar, args=("Possel-darwin-x64", "possel-osx-x64.tar.gz"))
	# Windows
	win32 = Process(target=make_zip, args=("Possel-win32-ia32", "possel-win32.zip"))
	win64 = Process(target=make_zip, args=("Possel-win32-x64", "possel-win64.zip"))

	tasks = [linux32, linux64, osx64, win32, win64]

	for task in tasks:
		task.start()

	all_complete = False
	while not all_complete:
		num_complete = 0
		for task in tasks:
			if not task.is_alive():
				num_complete += 1

		if num_complete == len(tasks):
			all_complete = True

def clean_environment():
	for tempoutput in ['Possel-linux-ia32', 'Possel-linux-x64', 'Possel-darwin-x64', 'Possel-win32-ia32', 'Possel-win32-x64']:
		shutil.rmtree(tempoutput)

	if not os.path.exists("output"):
		os.makedirs("output")

	for output in ['possel-linux-x32.tar.gz', 'possel-linux-x64.tar.gz', 'possel-osx-x64.tar.gz', 'possel-win32.zip', 'possel-win64.zip']:
		shutil.move(output, "output")

if __name__ == "__main__":
	if len(sys.argv) == 1:
		print("Please specify a target platform")
		sys.exit(1)

	target_platform = sys.argv[1]

	source_folder = "src"
	if len(sys.argv) > 2:
		source_folder = sys.argv[2]

	valid_platforms = [
		'linux',
		'win32',
		'darwin',
		'all'
	]

	if "," in target_platform:
		target_platforms = target_platform.split(",")
		for platform in target_platforms:
			if platform not in valid_platforms:
				print "%s is not a valid platform" % (platform,)
				sys.exit(2)
		target_platform = ",".join(target_platforms)
	else:
		if target_platform not in valid_platforms:
			print "%s is not a valid platform" % (target_platform,)
			sys.exit(2)

	print("Contacting GitHub to determine latest Electron release")
	latest_electron_version = get_latest_electron_version()

	print("Installing electron-packager")
	os.system("npm install electron-packager")

	print("Building with electron-packager")
	electron_packager = os.path.join("node_modules", ".bin", "electron-packager")
	os.system("%s %s Possel --platform=%s --arch=all --version=%s --asar --overwrite" % (electron_packager, source_folder, target_platform, latest_electron_version))
	print("Building Complete")

	print("Compressing builds")
	compress_builds()

	print("Compression complete, now tidying up")
	clean_environment()

	print("=== Building & Packaging Completed ===")
